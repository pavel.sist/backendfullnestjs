import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import Producto from './modules/producto/producto.entity';
import User from './modules/user/user.entity';
// import Venta from './modules/venta/venta.entity';
// import VentaDetalle from './modules/venta-detalle/venta-detalle.entity';

@Injectable()
class RepoService {
    public constructor(
        @InjectRepository(Producto) public readonly productoRepo: Repository<Producto>,
        @InjectRepository(User) public readonly userRepo: Repository<User>,
        // @InjectRepository(VentaDetalle) public readonly ventaDetalleRepo: Repository<VentaDetalle>,
    ) { }
}
export default RepoService;