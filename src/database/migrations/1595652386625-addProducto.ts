import {MigrationInterface, QueryRunner} from "typeorm";

export class addProducto1595652386625 implements MigrationInterface {
    name = 'addProducto1595652386625'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "productos" ("id" SERIAL NOT NULL, "idUsuarioCreac" integer, "idUsuarioModif" integer, "fechaCreacion" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "fechaModif" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "denominacion" character varying(50) NOT NULL, "codigo" character varying(100) NOT NULL, "codigoBarra" character varying(500) NOT NULL, "alias" character varying(300) NOT NULL, "precioVenta" numeric NOT NULL, "precioCompra" numeric NOT NULL, "tag" character varying(300) NOT NULL, "unidadMedida" character varying(200) NOT NULL, "idcTipoIgv" integer NOT NULL, "observacion" character varying(800) NOT NULL, "estado" boolean NOT NULL, CONSTRAINT "PK_04f604609a0949a7f3b43400766" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "productos"`);
    }

}
