import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import RepoService from './repo.service';
import Producto from './modules/producto/producto.entity';
import User from './modules/user/user.entity';
// import Venta from './modules/venta/venta.entity';
// import VentaDetalle from './modules/venta-detalle/venta-detalle.entity';

@Global()
@Module({
    imports: [
        TypeOrmModule.forFeature([
            Producto,
            User,
            //VentaDetalle
        ]),
    ],
    providers: [RepoService],
    exports: [RepoService],
})
class RepoModule {

}
export default RepoModule;