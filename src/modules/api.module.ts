import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { ProductoModule } from './producto/producto.module';
import { RolModule } from './rol/rol.module';


@Module({
    imports: [ProductoModule, RolModule, UserModule]
})
export class ApiModule { }
