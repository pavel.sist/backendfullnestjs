import { Args, Resolver, Query, Mutation } from '@nestjs/graphql';
import User from '../user.entity';
import RepoService from '../../../repo.service';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '../../../modules/auth/auth2/auth.guard';
// import { AuthGuard } from '@nestjs/passport';
// import { GqlAuthGuard } from '../../auth2/guards/gql.guard';


@Resolver(() => User)
export default class UserResolver {
    constructor(private readonly repoService: RepoService) { }

    //@UseGuards(new AuthGuard())
    // @UseGuards(GqlAuthGuard)
    @Query(() => [User])
    public async users(): Promise<User[]> {
        return this.repoService.userRepo.find();
    }

    // @Mutation()
    // async login(@Args('email') email: string) {
    //     let user = await this.repoService.userRepo.getUserByEmail(email);
    //     if (!user) {
    //         user = await this.repoService.userRepo.createUser(email);
    //     }
    //     return this.repoService.userRepo.createToken(user);
    // }



}
