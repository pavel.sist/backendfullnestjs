import { Entity, Column } from 'typeorm';
import { BaseEntity } from '../../../base.entity';

@Entity('user_details')
export default class UserDetails extends BaseEntity {

    @Column({ type: 'varchar', length: 50, nullable: false })
    name: string;

    @Column({ type: 'varchar', nullable: false })
    lastname: string;

    @Column({ type: 'varchar', default: 'ACTIVE', length: 8 })
    status: string;

}
