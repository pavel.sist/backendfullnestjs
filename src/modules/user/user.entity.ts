import { Entity, Column, OneToOne, JoinTable, ManyToMany, JoinColumn, BaseEntity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
// import { BaseEntity as BaseEntity1 } from '../../../base.entity';
import UserDetails from './user.details.entity';
import Rol from '../rol/rol.entity';
import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType('users')
@Entity('users')
export default class User extends BaseEntity {
    @Field()
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Field()
    @Column({ type: 'varchar', unique: true, length: 25, nullable: false })
    username: string;

    @Field()
    @Column({ type: 'varchar', nullable: false })
    email: string;

    @Field()
    @Column({ type: 'varchar', nullable: false })
    password: string;

    @Field()
    @Column({ type: 'varchar', default: 'ACTIVE', length: 8 })
    status: string;

    @Field()
    @Column({ nullable: true })
    idUsuarioCreac: number;

    @Field()
    @Column({ nullable: true })
    idUsuarioModif: number;

    @Field()
    @Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
    fechaCreacion: Date;

    @Field()
    @Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
    fechaModif: Date;

    @OneToOne(type => UserDetails,
        {
            cascade: true, nullable: false, eager: true
        })
    @JoinColumn({ name: 'detail_id' })
    details: UserDetails;

    @ManyToMany(type => Rol, rol => rol.users, { eager: true })
    @JoinTable({ name: "user_roles" })
    roles: Rol[];
}
