import { Controller, Get, Param, ParseIntPipe, Post, Body, Patch, Delete, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from './dto/user.dto';
import User from './user.entity';
import { AuthGuard } from '@nestjs/passport';

import { Roles } from '../rol/decorators/rol.decorator';
import { RolGuard } from '../rol/guards/rol.guard';
import { RoleType } from '../rol/roltype.enum';
// import { AuthGuard } from '../auth/auth2/auth.guard';

@Controller('users')
export class UserController {
    constructor(private readonly _userService: UserService) { }

    @UseGuards(AuthGuard())
    @Get()
    async getUsers(): Promise<UserDto[]> {
        return await this._userService.getAll();
    }

    @Get(':id')
    @Roles(RoleType.AUTHOR)
    @UseGuards(AuthGuard(), RolGuard)
    async getUser(@Param('id', ParseIntPipe) id: number): Promise<UserDto> {
        return await this._userService.get(id);

    }

    @Post('create')
    async createUser(@Body() user: User): Promise<UserDto> {
        const createdUser = await this._userService.create(user);
        return createdUser;
    }

    @Patch(':id')
    async updateUser(@Param('id', ParseIntPipe) id: number, @Body() user: User): Promise<UserDto> {
        const createdUser = await this._userService.update(id, user);
        return createdUser;

    }
    @Delete(':id')
    async deleteUser(@Param('id', ParseIntPipe) id: number) {
        await this._userService.delete(id);
        return true;
    }

    @Post('setRole/:userId/:roleId')
    async setRoleToUser(
        @Param('userId', ParseIntPipe) userId: number,
        @Param('roleId', ParseIntPipe) roleId: number,
    ) {
        return this._userService.setRoleToUser(userId, roleId);
    }

}
