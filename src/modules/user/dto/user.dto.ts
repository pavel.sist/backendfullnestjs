import { IsNotEmpty, IsString, IsNumber } from 'class-validator';
import { RoleType } from '../../rol/roltype.enum';
import UserDetails from '../user.details.entity';
import { Exclude, Expose } from 'class-transformer';
@Exclude()
export class UserDto {

    @Expose()
    @IsNotEmpty()
    @IsNumber()
    readonly id: number;

    @Expose()
    @IsNotEmpty()
    @IsString()
    readonly username: string;

    @Expose()
    @IsNotEmpty()
    readonly email: string;

    @Expose()
    @IsNotEmpty()
    roles: RoleType[];

    @Expose()
    @IsNotEmpty()
    details: UserDetails[];

}