import { Module } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { AuthModule } from '../auth/auth.module';
import { RolRepository } from '../rol/rol.repository';
import UserResolver from './resolvers/user.resolver';

@Module({
    imports: [TypeOrmModule.forFeature([UserRepository, RolRepository]),
        AuthModule
    ],
    providers: [UserService, UserResolver],
    controllers: [UserController],

})
export class UserModule { }
