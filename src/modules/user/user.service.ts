import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { UserDto } from './dto/user.dto';
import User from './user.entity';
import { plainToClass } from 'class-transformer';
import { status } from '../../shared/entity-status.num';
import { RolRepository } from '../rol/rol.repository';
// import * as jwt from 'jsonwebtoken';
@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserRepository)
        private readonly _userRepository: UserRepository,
        @InjectRepository(RolRepository)
        private readonly _roleRepository: RolRepository,

    ) { }
    async getAll(): Promise<UserDto[]> {
        const users: User[] = await this._userRepository.find(
            { where: { status: status.ACTIVE }, });
        return users.map((user: User) => plainToClass(UserDto, user));
    }

    async get(id: number): Promise<UserDto> {
        if (!id) {
            throw new BadRequestException('id must be sent');
        }
        const user: User = await this._userRepository.findOne(id, {
            where: { status: status.ACTIVE },
        });

        if (!user) {
            throw new NotFoundException();
        }
        return plainToClass(UserDto, user);
    }

    async create(user: Partial<User>): Promise<UserDto> {
        const saveUser: User = await this._userRepository.save(user);
        return plainToClass(UserDto, saveUser);
    }
    async update(id: number, user: Partial<User>): Promise<UserDto> {
        const foundUser = await this._userRepository.findOne(id, {
            where: { status: status.ACTIVE }
        });
        if (!foundUser) {
            throw new NotFoundException('this producto does not exist');
        }
        foundUser.username = user.username;
        foundUser.email = user.email;

        const updateUser: User = await this._userRepository.save(foundUser);
        return plainToClass(UserDto, updateUser);
    }

    async delete(id: number): Promise<void> {
        const userExist = await this._userRepository.findOne(id, {
            where: { status: status.ACTIVE },
        });
        if (!userExist) {
            throw new NotFoundException;
        }
        await this._userRepository.update(id, { status: 'INACTIVE' });
    }

    async setRoleToUser(userId: number, roleId: number) {
        const userExist = await this._userRepository.findOne(userId, {
            where: { status: status.ACTIVE },
        });

        if (!userExist) {
            throw new NotFoundException();
        }

        const roleExist = await this._roleRepository.findOne(roleId, {
            where: { status: status.ACTIVE },
        });

        if (!roleExist) {
            throw new NotFoundException('Role does not exist');
        }

        userExist.roles.push(roleExist);
        await this._userRepository.save(userExist);

        return true;
    }


    // createToken({ id, email }: User) {
    //     const user: UserModel = { id, email };
    //     const secret = config.get(Configuration.JWT_SECRET);
    //     return jwt.sign(user, secret);
    //   }

    //   createUser(email: string) {
    //     return this.userRepo.create({ email }).save();
    //   }

    //   getUserByEmail(email: string) {
    //     return this.userRepo.findOne({ email });
    //   }


}
