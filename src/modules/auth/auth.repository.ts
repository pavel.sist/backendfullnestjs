import { Repository, EntityRepository, getConnection } from "typeorm";
import User from '../user/user.entity';
import { SignupDto } from "./dto";
import { RolRepository } from '../rol/rol.repository';
import Rol from "../rol/rol.entity";
import { RoleType } from '../rol/roltype.enum';
import UserDetails from "../user/user.details.entity";
import { genSalt, hash } from "bcryptjs";

@EntityRepository(User)
export class AuthRepository extends Repository<User>{
    async signup(signupDto: SignupDto) {
        const { username, email, password } = signupDto;
        const user = new User();
        user.username = username;
        user.email = email;

        const roleRepository: RolRepository = await getConnection()
            .getRepository(
                Rol
            );
        const defaultRol: Rol = await roleRepository.findOne({
            where: { name: RoleType.GENERAL },
        });

        user.roles = [defaultRol];

        const details = new UserDetails();
        user.details = details;
        user.details.name = "-";
        user.details.lastname = "-";
        user.details.status = "ACTIVE";


        const salt = await genSalt(10);
        user.password = await hash(password, salt);

        await user.save();
    }
}