import { Injectable, ConflictException, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { AuthRepository } from './auth.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { SignupDto, SigninDto } from './dto';
import { compare } from 'bcryptjs';
import { IJwtPayload } from './jwt-payload.interface';
import { RoleType } from '../rol/roltype.enum';
import User from '../user/user.entity';
import { plainToClass } from 'class-transformer';
import { LoggedInDto } from './dto/logged-in.dto';

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(AuthRepository)
        private readonly _authRepository: AuthRepository,
        private readonly _jwtService: JwtService,
    ) { }

    async signup(signupDto: SignupDto): Promise<void> {
        const { username, email } = signupDto;
        const userExist = await this._authRepository.findOne({
            where: [{ username }, { email }]
        });

        if (userExist) {
            throw new ConflictException('username or email already exists');
        }

        return this._authRepository.signup(signupDto);
    }

    async signin(signinDto: SigninDto): Promise<LoggedInDto> {
        const { username, password } = signinDto;
        const user: User = await this._authRepository.findOne({
            where: { username },
        });
        if (!user) {
            throw new NotFoundException('username dos not exist');
        };

        const isMatch = await compare(password, user.password);

        if (!isMatch) {
            throw new UnauthorizedException('ivalid credentials');
        };

        const payload: IJwtPayload = {
            id: user.id,
            email: user.email,
            username: user.username,
            roles: user.roles.map(r => r.name as RoleType)
        };

        const token = await this._jwtService.sign(payload);

        return plainToClass(LoggedInDto, { token, user });
    }
}
