import { HttpStatus, HttpException, CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import * as jwt from 'jsonwebtoken';
import { ConfigService } from '../../../config/config.service';
import { Configuration } from '../../../config/config.keys';

import { JwtService } from '@nestjs/jwt';
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        // private readonly _jwtService: JwtService
        //  private readonly _configService: ConfigService,
        //private readonly _jwtService: JwtService,
    ) { }

    canActivate(context: ExecutionContext): boolean {
        const ctx = GqlExecutionContext.create(context).getContext();
        if (!ctx.headers.authorization) {
            return false;
        }
        ctx.user = this.validateToken(ctx.headers.authorization);
        return true;
    }

    validateToken(auth: string) {
        if (auth.split(' ')[0] !== 'Bearer') {
            throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
        }
        const token = auth.split(' ')[1];
        try {
            // const secret = Configuration.JWT_SECRET;
            return jwt.verify(token, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJlcmlrQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoiZXJpayIsInJvbGVzIjpbIkdFTkVSQUwiLCJBVVRIT1IiXSwiaWF0IjoxNTk2MTcxNTUzLCJleHAiOjE1OTYxNzUxNTN9.KeuBKFPoFbpKW8Uqf14e3T8DhzkTvXBVYOCmG-7NV6Q");
        } catch (err) {
            throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
        }
    }
}