import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common';
import { ProductoService } from './producto.service';
import { ProductoDto } from './dto';

@Controller('productos')
export class ProductoController {
    constructor(private readonly _productoService: ProductoService) { }

    @Get(':id')
    async getProducto(@Param('id', ParseIntPipe) id: number): Promise<ProductoDto> {
        return await this._productoService.get(id);
    }

    @Get()
    async getProductos(): Promise<ProductoDto[]> {
        return await this._productoService.getAll();
    }
}
