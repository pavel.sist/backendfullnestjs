import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import Producto from '../producto.entity';
import RepoService from '../../../repo.service';
import ProductoInput from './input/producto.input';
import { UseGuards } from '@nestjs/common';
// import { AuthGuard } from '../../auth/auth2/auth.guard';
// import { AuthGuard } from '../../auth/auth2/auth.guard';
// import { AuthGuard } from '@nestjs/passport';

@Resolver(() => Producto)
class ProductoResolver {
    constructor(private readonly repoService: RepoService) { }

    // @UseGuards(AuthGuard())
    @Query(() => [Producto])
    public async productosAuth(@Args('denominacion') denominacion: string): Promise<Producto[]> {
        return this.repoService.productoRepo.createQueryBuilder("productos")
            .where("LOWER(productos.denominacion) LIKE :denominacion", { denominacion: `%${denominacion.toLowerCase()}%` })
            .orWhere("LOWER(productos.codigoBarra) LIKE :codigoBarra", { codigoBarra: `%${denominacion.toLowerCase()}%` })
            .getMany();
    }

    @Query(() => [Producto])
    public async productos(@Args('denominacion') denominacion: string): Promise<Producto[]> {
        return this.repoService.productoRepo.createQueryBuilder("productos")
            .where("LOWER(productos.denominacion) LIKE :denominacion", { denominacion: `%${denominacion.toLowerCase()}%` })
            .orWhere("LOWER(productos.codigoBarra) LIKE :codigoBarra", { codigoBarra: `%${denominacion.toLowerCase()}%` })
            .getMany();
    }

    @Query(() => Producto, { nullable: true })
    public async producto(@Args('id') id: number): Promise<Producto> {
        return this.repoService.productoRepo.findOne(
            {
                where: { id },
                //relations: ['ventaDetalle']
            });
    }

    @Mutation(() => Producto)
    public async createProducto(@Args('data') input: ProductoInput):
        Promise<Producto> {
        const producto = this.repoService.productoRepo.create({
            idUsuarioCreac: input.idUsuarioCreac,
            codigo: input.codigo,
            denominacion: input.denominacion,
            codigoBarra: input.codigoBarra,
            alias: input.alias,
            precioVenta: input.precioVenta,
            precioCompra: input.precioCompra,
            tag: input.tag,
            unidadMedida: input.unidadMedida,
            idcTipoIgv: input.idcTipoIgv,
            estado: input.estado,
            observacion: input.observacion
        });
        return this.repoService.productoRepo.save(producto);
    }

    @Mutation(() => Boolean)
    public async deleteProducto(@Args('id') id: number):
        Promise<Boolean> {
        try {
            await this.repoService.productoRepo.delete(id);


        } catch (error) {
            return false;
        }
        return true;

    }


}

export default ProductoResolver;
