import { Field, InputType } from '@nestjs/graphql'

@InputType()
class ProductoInput {
    @Field()
    readonly idUsuarioCreac: number;
    @Field()
    readonly codigo: string;
    @Field()
    readonly denominacion: string;
    @Field()
    readonly codigoBarra: string;
    @Field()
    readonly alias: string;
    @Field()
    readonly precioVenta: number;
    @Field()
    readonly precioCompra: number;
    @Field()
    readonly tag: string;
    @Field()
    readonly unidadMedida: string;
    @Field()
    readonly idcTipoIgv: number;
    @Field()
    readonly observacion: string;
    @Field()
    readonly estado: boolean;
}

export default ProductoInput;