import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductoRepository } from './producto.repository';
import { ProductoDto } from './dto/index';
import Producto from './producto.entity';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ProductoService {
    constructor(
        @InjectRepository(ProductoRepository)
        private readonly _productoRepository: ProductoRepository
    ) { }

    async get(id: number): Promise<ProductoDto> {
        if (!id) {
            throw new BadRequestException('id must be sent');
        }
        const producto: Producto = await this._productoRepository.findOne(id, {
            where: { estado: true },
        })
        if (!producto) {
            throw new NotFoundException();
        }
        return plainToClass(ProductoDto, producto);
    }

    async getAll(): Promise<ProductoDto[]> {
        const productos: Producto[] = await this._productoRepository.find(
            {
                where: { estado: true },
            });
        return productos.map((producto: Producto) => plainToClass(ProductoDto, producto));
    }
}
