import { Module } from '@nestjs/common';
import { ProductoController } from './producto.controller';
import { ProductoService } from './producto.service';
import ProductoResolver from './resolvers/producto.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductoRepository } from './producto.repository';
import { AuthModule } from '../auth/auth.module';
import { AuthGuard } from '../auth/auth2/auth.guard';

@Module({
  imports: [TypeOrmModule.forFeature([ProductoRepository]),
    AuthModule, AuthGuard
  ],
  controllers: [ProductoController],
  providers: [ProductoService, ProductoResolver]
})
export class ProductoModule { }
