import { Entity, Column } from 'typeorm';
import { BaseEntity } from '../../../base.entity';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType('productos')
@Entity('productos')
export default class Producto extends BaseEntity {
    @Field()
    @Column({ type: 'varchar', length: 50, nullable: false })
    denominacion: string;

    @Field()
    @Column({ type: 'varchar', length: 100, nullable: false })
    codigo: string;

    @Field()
    @Column({ type: 'varchar', length: 500, nullable: false })
    codigoBarra: string;

    @Field()
    @Column({ type: 'varchar', length: 300, nullable: false })
    alias: string;

    @Field()
    @Column({ type: 'decimal', scale: 3, nullable: false })
    precioVenta: number;

    @Field()
    @Column({ type: 'decimal', scale: 3, nullable: false })
    precioCompra: number;

    @Field()
    @Column({ type: 'varchar', length: 300, nullable: false })
    tag: string;

    @Field()
    @Column({ type: 'varchar', length: 200, nullable: false })
    unidadMedida: string;

    @Field()
    @Column({ type: 'int', nullable: false })
    idcTipoIgv: number;

    @Field()
    @Column({ type: 'varchar', length: 800, nullable: false })
    observacion: string;

    @Field()
    @Column({ type: 'boolean', nullable: false })
    estado: boolean;

    // @OneToMany(type => VentaDetalle, ventaDetalle => ventaDetalle.producto)
    // productoItem: VentaDetalle[];



    // @Column({ type: 'float', nullable: false, default: 0.0 })
    // cantt: number;
    // @Column({ type: 'boolean', nullable: false, default: true })
    // conn: boolean;


}
