import { IsNotEmpty, IsString, MaxLength, IsNumber } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
@Exclude()
export class ProductoDto {
    @Expose()
    @IsNotEmpty()
    @IsNumber()
    readonly id: number;

    @Expose()
    @IsNotEmpty()
    @IsString()
    readonly codigo: string;

    @Expose()
    @IsNotEmpty()
    readonly codigoBarra: string;

    @Expose()
    @IsNotEmpty()
    readonly denominacion: string;

    @Expose()
    @IsNotEmpty()
    @MaxLength(100, { message: '<DTO-MSSG>MAXIMO 100 CARACTERES' })
    readonly alias: string;

    @Expose()
    @IsNotEmpty()
    readonly estado: boolean;

}