import { IsString, MaxLength, IsNumber } from 'class-validator';

import { Exclude, Expose } from 'class-transformer';


export class UpdateRolDto {
    @IsString()
    @MaxLength(50, { message: 'no puede sr mayor a 50 length' })
    readonly name: string;

    @IsString()
    @MaxLength(100, { message: 'no puede sr mayor a 100 length' })
    readonly descripcion: string;

}
