import { IsString, MaxLength, IsNumber } from 'class-validator';

import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class ReadRolDto {
    @Expose()
    @IsNumber()
    readonly id: number;

    @Expose()
    @IsString()
    @MaxLength(50, { message: 'no puede sr mayor a 50 length' })
    readonly name: string;

    @Expose()
    @IsString()
    @MaxLength(100, { message: 'no puede sr mayor a 100 length' })
    readonly descripcion: string;



}
