import { IsNotEmpty, IsString, IsNumber } from 'class-validator';
// import { RoleType } from '../roltype.enum';
import { Exclude, Expose } from 'class-transformer';
@Exclude()
export class RolDto {

    @Expose()
    @IsNotEmpty()
    @IsNumber()
    readonly id: number;

    @Expose()
    @IsNotEmpty()
    @IsString()
    readonly name: string;

    @Expose()
    @IsNotEmpty()
    readonly descripcion: string;

    // @Expose()
    // @IsNotEmpty()
    // roles: RoleType[];

    // @Expose()
    // @IsNotEmpty()
    // details: UserDetails[];

}