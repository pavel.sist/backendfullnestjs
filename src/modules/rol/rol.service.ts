import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RolRepository } from './rol.repository';
import { RolDto } from './dto/rol.dto';
import Rol from './rol.entity';
import { plainToClass } from 'class-transformer';
import { ReadRolDto, CreateRolDto } from './dto';
import { status } from '../../shared/entity-status.num';
import { UpdateRolDto } from './dto/update-rol.dto';

@Injectable()
export class RolService {
    constructor(
        @InjectRepository(RolRepository)
        private readonly _rolRepository: RolRepository
    ) { }
    async getAll(): Promise<ReadRolDto[]> {
        const rols: Rol[] = await this._rolRepository.find(
            { where: { status: status.ACTIVE }, });
        return rols.map((rol: Rol) => plainToClass(ReadRolDto, rol));
    }

    async get(id: number): Promise<ReadRolDto> {
        if (!id) {
            throw new BadRequestException('id must be sent');
        }
        const rol: Rol = await this._rolRepository.findOne(id, {
            where: { status: status.ACTIVE },
        });

        if (!rol) {
            throw new NotFoundException();
        }
        return plainToClass(ReadRolDto, rol);
    }

    async create(rol: Partial<CreateRolDto>): Promise<ReadRolDto> {
        const saveRol: Rol = await this._rolRepository.save(rol);
        return plainToClass(ReadRolDto, saveRol);
    }
    async update(id: number, rol: Partial<UpdateRolDto>): Promise<ReadRolDto> {
        const foundRol = await this._rolRepository.findOne(id, {
            where: { status: status.ACTIVE }
        });
        if (!foundRol) {
            throw new NotFoundException('this producto does not exist');
        }
        foundRol.name = rol.name;
        foundRol.descripcion = rol.descripcion;

        const updateRol: Rol = await this._rolRepository.save(foundRol);
        return plainToClass(ReadRolDto, updateRol);
    }

    async delete(id: number): Promise<void> {
        const rolExist = await this._rolRepository.findOne(id, {
            where: { status: status.ACTIVE },
        });
        if (!rolExist) {
            throw new NotFoundException;
        }
        await this._rolRepository.update(id, { status: status.INACTIVE });
    }




}
