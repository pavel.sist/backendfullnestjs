import { Controller, Get, Param, ParseIntPipe, Post, Body, Patch, Delete } from '@nestjs/common';
import { RolService } from './rol.service';
import { ReadRolDto, CreateRolDto, UpdateRolDto } from './dto';

@Controller('rols')
export class RolController {
    constructor(private readonly _rolService: RolService) { }
    @Get()
    async getRols(): Promise<ReadRolDto[]> {
        return await this._rolService.getAll();
    }

    @Get(':id')
    async getRol(@Param('id', ParseIntPipe) id: number): Promise<ReadRolDto> {
        return await this._rolService.get(id);

    }

    @Post('create')
    async createRol(@Body() rol: Partial<CreateRolDto>): Promise<ReadRolDto> {
        return await this._rolService.create(rol);

    }

    @Patch(':id')
    async updateRol(
        @Param('id', ParseIntPipe) id: number,
        @Body() rol: Partial<UpdateRolDto>): Promise<ReadRolDto> {
        return await this._rolService.update(id, rol);

    }
    @Delete(':id')
    async deleteRol(@Param('id', ParseIntPipe) id: number) {
        await this._rolService.delete(id);
        return true;
    }
}
