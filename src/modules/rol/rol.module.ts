import { Module } from '@nestjs/common';
import { RolRepository } from './rol.repository';
import { RolService } from './rol.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolController } from './rol.controller';

@Module({
    imports: [TypeOrmModule.forFeature([RolRepository])],
    providers: [RolService],
    controllers: [RolController
    ],
})
export class RolModule { }
