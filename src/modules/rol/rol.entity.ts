import { Entity, Column, ManyToMany, JoinColumn } from 'typeorm';
import { BaseEntity } from '../../../base.entity';
import User from '../user/user.entity';



@Entity('roles')
export default class Rol extends BaseEntity {

    @Column({ type: 'varchar', nullable: false })
    name: string;

    @Column({ type: 'varchar', nullable: false })
    descripcion: string;

    @Column({ type: 'varchar', default: 'ACTIVE', length: 8 })
    status: string;

    @ManyToMany(type => User, user => user.roles)
    @JoinColumn()
    users: User[]

}
