import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { Configuration } from './config/config.keys';
import { DatabaseModule } from './database/database.module';
import { AuthModule } from './modules/auth/auth.module';
import ProductoResolver from './modules/producto/resolvers/producto.resolver';
import UserResolver from './modules/user/resolvers/user.resolver';
import RepoModule from './repo.module';
import { GraphQLModule } from '@nestjs/graphql';
import { ApiModule } from './modules/api.module';

const graphQLImports = [
  ProductoResolver,
  UserResolver,
  // VentaDetalleResolver
];

@Module({
  imports: [
    RepoModule,
    ...graphQLImports,
    GraphQLModule.forRoot({
      // typePaths: ['./**/*.graphql'],
      autoSchemaFile: 'schema.gql',
      playground: true,
      context: ({ req }) => ({ headers: req.headers }),
      //context: {
      // ventaDetalleLoader: ventaDetalleLoader(),
      //},

    }),
    ConfigModule,
    DatabaseModule,
    ApiModule,
    AuthModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static port: number | string;
  constructor(private readonly _configService: ConfigService) {
    AppModule.port = this._configService.get(Configuration.PORT)
  }
}
