import * as fs from 'fs';
// import { parse } from 'dotenv';


export class ConfigService {
    private readonly envConfig: { [key: string]: string };

    constructor() {
        console.log("-------------object--------------")
        const idDevelopmentEnv = process.env.NODE_ENV !== 'production';

        if (idDevelopmentEnv) {
            console.log("-------------idDevelopmentEnv--------------")
            const dotenv = require('dotenv');
            const envFilePath = __dirname + '/../../.env';
            const existPath = fs.existsSync(envFilePath);

            if (!existPath) {

                console.log('.env file does not exist');
                process.exit(0);
            }

            this.envConfig = dotenv.parse(fs.readFileSync(envFilePath));
        } else {
            console.log("-------------isproduction--------------")
            this.envConfig = {
                PORT: process.env.PORT,
                HOST: process.env.HOST,
                USERNAME: process.env.USERNAME,
                PASSWORD: process.env.PASSWORD,
                DATABASE: process.env.DATABASE,
                JWT_SECRET: process.env.JWT_SECRET,
            }
        }
    }

    get(key: string): string {
        return this.envConfig[key];
    }
}